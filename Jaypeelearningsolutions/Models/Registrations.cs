﻿using System;

namespace Jaypeelearningsolutions.Models
{
    public class Registrations
    {
        public int Id { get; set; }
        public String UserName { get; set; }
        public string Email { get; set; }
        public String MobileNumber { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public int PinCode { get; set; }
        public string Workplace { get; set; }
        public string Speciality { get; set; }
        public string Registredfrom { get; set; }
        public string AreaofIntrest { get; set; }
        public string Password { get; set; }
        public string Deviece { get; set; }
        public string Browser { get; set; }
        public bool IsActivated { get; set; } = true;
        public bool IsDeleted { get; set; } = false;
        public bool IsAdvanceUser { get; set; } = false;
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string UpdatedBy { get; set; }
    }
}